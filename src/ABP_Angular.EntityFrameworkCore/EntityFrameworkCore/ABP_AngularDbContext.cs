﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using ABP_Angular.Authorization.Roles;
using ABP_Angular.Authorization.Users;
using ABP_Angular.MultiTenancy;

namespace ABP_Angular.EntityFrameworkCore
{
    public class ABP_AngularDbContext : AbpZeroDbContext<Tenant, Role, User, ABP_AngularDbContext>
    {
        /* Define a DbSet for each entity of the application */
        
        public ABP_AngularDbContext(DbContextOptions<ABP_AngularDbContext> options)
            : base(options)
        {
        }
    }
}
