﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using ABP_Angular.Configuration;
using ABP_Angular.Web;

namespace ABP_Angular.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class ABP_AngularDbContextFactory : IDesignTimeDbContextFactory<ABP_AngularDbContext>
    {
        public ABP_AngularDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<ABP_AngularDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            ABP_AngularDbContextConfigurer.Configure(builder, configuration.GetConnectionString(ABP_AngularConsts.ConnectionStringName));

            return new ABP_AngularDbContext(builder.Options);
        }
    }
}
