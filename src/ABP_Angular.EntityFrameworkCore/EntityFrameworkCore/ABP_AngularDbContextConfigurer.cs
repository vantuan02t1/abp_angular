using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace ABP_Angular.EntityFrameworkCore
{
    public static class ABP_AngularDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<ABP_AngularDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<ABP_AngularDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
