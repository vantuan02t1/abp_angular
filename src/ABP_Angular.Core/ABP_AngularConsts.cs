﻿namespace ABP_Angular
{
    public class ABP_AngularConsts
    {
        public const string LocalizationSourceName = "ABP_Angular";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
