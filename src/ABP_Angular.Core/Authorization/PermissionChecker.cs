﻿using Abp.Authorization;
using ABP_Angular.Authorization.Roles;
using ABP_Angular.Authorization.Users;

namespace ABP_Angular.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
