using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace ABP_Angular.Controllers
{
    public abstract class ABP_AngularControllerBase: AbpController
    {
        protected ABP_AngularControllerBase()
        {
            LocalizationSourceName = ABP_AngularConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
