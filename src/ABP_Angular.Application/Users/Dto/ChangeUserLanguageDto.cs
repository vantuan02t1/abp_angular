using System.ComponentModel.DataAnnotations;

namespace ABP_Angular.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}