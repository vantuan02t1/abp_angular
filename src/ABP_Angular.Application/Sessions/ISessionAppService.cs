﻿using System.Threading.Tasks;
using Abp.Application.Services;
using ABP_Angular.Sessions.Dto;

namespace ABP_Angular.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
