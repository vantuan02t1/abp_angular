﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using ABP_Angular.Authorization;

namespace ABP_Angular
{
    [DependsOn(
        typeof(ABP_AngularCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class ABP_AngularApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<ABP_AngularAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(ABP_AngularApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddMaps(thisAssembly)
            );
        }
    }
}
