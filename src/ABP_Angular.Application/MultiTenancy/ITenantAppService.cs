﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ABP_Angular.MultiTenancy.Dto;

namespace ABP_Angular.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

