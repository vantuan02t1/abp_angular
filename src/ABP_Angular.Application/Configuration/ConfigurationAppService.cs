﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using ABP_Angular.Configuration.Dto;

namespace ABP_Angular.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : ABP_AngularAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
