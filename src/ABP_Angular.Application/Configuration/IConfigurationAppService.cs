﻿using System.Threading.Tasks;
using ABP_Angular.Configuration.Dto;

namespace ABP_Angular.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
