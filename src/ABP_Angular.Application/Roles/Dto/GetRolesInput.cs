﻿namespace ABP_Angular.Roles.Dto
{
    public class GetRolesInput
    {
        public string Permission { get; set; }
    }
}
