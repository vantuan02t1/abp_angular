import { ABP_AngularTemplatePage } from './app.po';

describe('ABP_Angular App', function() {
  let page: ABP_AngularTemplatePage;

  beforeEach(() => {
    page = new ABP_AngularTemplatePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
