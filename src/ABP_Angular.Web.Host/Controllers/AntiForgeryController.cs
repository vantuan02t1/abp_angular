using Microsoft.AspNetCore.Antiforgery;
using ABP_Angular.Controllers;

namespace ABP_Angular.Web.Host.Controllers
{
    public class AntiForgeryController : ABP_AngularControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
